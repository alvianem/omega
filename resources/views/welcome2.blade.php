<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/maskot-lkti.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keyword" content="lomba, ipa, um, universitas, smp, mts, malang, universitas negeri malang, omega, lkti, karya tulis ilmiah">
    <meta name="description" content="LKTI UM adalah bentuk partisipasi
    nyata dalam upaya meningkatkan kualitas pendidikan di negeri ini untuk
    mewujudkan generasi yang aktif, kreatif, dan penuh inovasi terhadap
    lingkungan guna menuju Indonesia yang lebih baik.
">
    <meta name="author" content="Olimpiade.id">
    <meta property="og:url" content="index.html" />
    <meta property="og:image" content="assets/img/maskot-lkti.png" />
    <meta content='#fff' name='theme-color'/>
    <meta content='Indonesia' name='geo.placename'/>

    <title>LKTI UM</title>

    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css'>

    <link href="assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="assets/css/freelancer2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style1.css">
    <link rel="stylesheet" href="assets/css/aos.css">

    <script type="text/javascript"> (function() { var css = document.createElement('link'); css.href = 'http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css'; css.rel = 'stylesheet'; css.type = 'text/css'; document.getElementsByTagName('head')[0].appendChild(css); })(); </script>

    <style type="text/css">
      .caption-animate .carousel-item.active .carousel-caption {
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
      }
      .caption-animate  .carousel-item.active .carousel-caption.infinite {
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
      }

      .caption-animate  .carousel-item.active .carousel-caption.hinge {
        -webkit-animation-duration: 2s;
        animation-duration: 2s;
      }

      .caption-animate .carousel-item.active .carousel-caption.flipOutX,
      .caption-animate .carousel-item.active .carousel-caption.flipOutY,
      .caption-animate .carousel-item.active .carousel-caption.bounceIn,
      .caption-animate .carousel-item.active .carousel-caption.bounceOut {
        -webkit-animation-duration: .75s;
        animation-duration: .75s;
      }
      .caption-animate .carousel-item.active .carousel-caption.fadeIn,
      .caption-animate .carousel-item.active .carousel-caption.fadeInDown,
      .caption-animate .carousel-item.active .carousel-caption.fadeInDownBig,
      .caption-animate .carousel-item.active .carousel-caption.fadeInLeft,
      .caption-animate .carousel-item.active .carousel-caption.fadeInLeftBig,
      .caption-animate .carousel-item.active .carousel-caption.fadeInRight,
      .caption-animate .carousel-item.active .carousel-caption.fadeInRightBig,
      .caption-animate .carousel-item.active .carousel-caption.fadeInUp,
      .caption-animate .carousel-item.active .carousel-caption.fadeInUpBig{
        opacity:0;
      }
    </style>

    <style type="text/css">
    #hilang{
      display: none;
    }
    #clockdiv{
    font-family: sans-serif;
    color: #fff;
    display: inline-block;
    font-weight: 100;
    text-align: center;
    font-size: 30px;
    }

    #clockdiv > div{
      padding: 10px;
      border-radius: 3px;
      background: #f28e1e;
      display: inline-block;
    }

    #clockdiv div > span{
      padding: 15px;
      border-radius: 3px;
      background: #ce432c;
      display: inline-block;
    }

    .smalltext{
      padding-top: 5px;
      font-size: 16px;
      font-weight: bold;
      font-family: Lato;
    }
    @media only screen and ( max-width : 769px ){
      #hilang{
        display: block;
      }
        #laptop{
          display: none;
        }
        #hp{
          display: block;
        }
        #my-slider {
          display: none;
        }
        #image_full{
         display:none;
        }

        #image_mobile{
         display:block;
        }
      }
    @media only screen and ( min-width : 769px ){
        #laptop{
          display: block;
        }
        #hp{
          display: none;
        }
        #my-slider2 {
          display: none;
        }
        #image_full{
           display:block;
          }

         #image_mobile{
          display:none;
         }
      }
    </style>

  </head>

  <body id="page-top">
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">OMEGA</a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">About</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#timeline">Timeline</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#prize">Prize</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
                <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#gallery">Gallery</a>
              </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#">Register</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead bg-primary text-white text-center" style="background: url('assets/img/bg-lkti.jpg') no-repeat center center fixed;background-size: cover;">
      <div class="container">
        <center><h1 class="text-uppercase mb-0" style="color:#71bd39;">Lomba<br> Karya Tulis Ilmiah</h1></center>
        <br>
        <h1 class="font-weight-light mb-0" style="color:#f28e1e;">2018</h1>
        <div class="text-center" style="padding-top: 25px">
            <a class="btn btn-xl btn-outline-light" style="border-color:#ffffff;color:#ffffff;border-width: 6px;margin: 10px" id="buttons" href="http://lkti.osd-hmpipaum.com/">
            <i class="fa fa-pencil-square-o mr-2" style="color:#ffffff"></i>
            Daftar <b>Disini</b>!
          </a>
          <a class="btn btn-xl btn-outline-light" href="assets/file/PETUNJUK PENULISAN.pdf" style="border-color:#ffffff;color:#ffffff;border-width: 6px;margin: 10px" id="buttons" download>
            <i class="fa fa-pencil-square-o mr-2" style="color:#ffffff"></i>
            Download <b>Petunjuk</b>!
          </a>
        </div>
      </div>
    </header>

    <section class="portfolio" style="padding-bottom:150px">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-12">
            <center><h1 style="color:#4f4f4f">Penutupan Pendaftaran</h1>
              <div id="clockdiv">
                  <div>
                    <span class="days"></span>
                    <div class="smalltext">Days</div>
                  </div>
                  <div>
                    <span class="hours"></span>
                    <div class="smalltext">Hours</div>
                  </div>
                  <div>
                    <span class="minutes"></span>
                    <div class="smalltext">Minutes</div>
                  </div>
                  <div>
                    <span class="seconds"></span>
                    <div class="smalltext">Seconds</div>
                  </div>
                </div>
            
          </div></center>
        </div>
      </div>
    </section>

    <section class="bg-primary text-white mb-0" style="background-color: #71bd39!important" id="about">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">Apa itu LKTI?</h2>
        <hr class="star-light mb-5">
        <div class="row">
          <div class="col-lg-4 ml-auto">
            <br>
            <center><img src="assets/img/maskot-lkti.png" style="width: 75%;text-align: center;"  data-aos="flip-up"></center>
            <br>
          </div>
          <div class="col-lg-7 ml-auto">
            <p class="lead" style="text-align: justify;">Diselenggarakan sebagai bentuk partisipasi
                nyata dalam upaya meningkatkan kualitas pendidikan di negeri ini untuk
                mewujudkan generasi yang aktif, kreatif, dan penuh inovasi terhadap
                lingkungan guna menuju Indonesia yang lebih baik. <br><br>Dalam kegiatan ini,
              terdapat satu rangkaian agenda yang akan dilaksanakan, yaitu Lomba Karya
              Tulis Ilmiah untuk siswa SMP/MTs se-Jawa Bali yang kemudian dilanjutkan
              dengan presentasi hasil karya tulis ilmiah. <br></br>Dengan adanya kegiatan ini, kami
              mengharapkan adanya hubungan yang sinergis antara aktivitas akademik
              Perguruan Tinggi dengan masyarakat umum dalam bebagai ilmu pengetahuan
              demi memajukan Indonesia.</p><br>
      <p class="lead" style="text-align: center;font-weight: bold">“Tema : Wujudkan Generasi Muda Berjiwa Saintis Melalui
          Karya Tulis yang Realistis, Inovatif, dan Kreatif”</p>
          </div>
          <div class="col-lg-1 ml-auto">
          </div>
      </div>
    </section>

    <section class="portfolio" id="requirement">
        <div class="container">
          <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#4f4f4f!important">Syarat & Ketentuan</h2>
          <hr class="star-dark mb-5" style="border-color: #4f4f4f">
          <div class="row">
            <div class="col-lg-12 ml-auto">
              <p class="lead" style="text-align: left;" data-aos="zoom-out-down">• Peserta adalah siswa tingkat SMP/MTs sederajat yang terdaftar di
                  SMP/MTs sederajat se-Jawa Bali.</p>
              <p class="lead" style="text-align: left;" data-aos="zoom-out-down">• LKTI diikuti oleh kelompok (terdiri dari maksimal 3 orang, minimal 2
                  orang) dari satu sekolah SMP/MTs sederajat se-Jawa Bali.</p>
              <p class="lead" style="text-align: left;" data-aos="zoom-out-down">• Tiap sekolah tidak dibatasi jumlah grup/tim/kelompok yang akan
                  diikutsertakan dalam lomba.</p>
              <p class="lead" style="text-align: left;" data-aos="zoom-out-down">• Setiap peserta (kelompok) hanya diperkenankan menulis dan
                  mengirimkan satu judul LKTI.</p>
            </div>
          </div>
        </div>
      </section>

    <section class="bg-primary text-white mb-0" id="timeline" style="background-color: #ce432c!important">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">Timeline</h2>
        <hr class="star-light2 mb-5">
        <section id="cd-timeline" class="cd-container">
    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-picture" style="background-color: #f28e1e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#71bd39">Pendaftaran</h4>
        <p style="color:#4f4f4f" id="hilang"><i>10 Juni - 25 Juni 2018</i></p>
        <p style="color:#4f4f4f">Gelombang 1</p>
        <span class="cd-date" style="opacity: 1">10 Juni - 25 Juni 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-movie" style="background-color: #f28e1e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#71bd39">Pendaftaran</h4>
        <p style="color:#4f4f4f" id="hilang"><i>26 Juni – 15 Juli 2018</i></p>
        <p style="color:#4f4f4f">Gelombang 2</p>
        <span class="cd-date" style="opacity: 1">26 Juni – 15 Juli 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-movie" style="background-color: #f28e1e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#71bd39">Pendaftaran</h4>
        <p style="color:#4f4f4f" id="hilang"><i>16 Juli - 26 Agustus 2018</i></p>
        <p style="color:#4f4f4f">Gelombang 3</p>
        <span class="cd-date" style="opacity: 1">16 Juli - 26 Agustus 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-movie" style="background-color: #f28e1e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#71bd39">Proses Seleksi Karya Tulis</h4>
        <p style="color:#4f4f4f" id="hilang"><i>27 Agustus – 2 September 2018</i></p>
        <span class="cd-date" style="opacity: 1">27 Agustus – 2 September 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-movie" style="background-color: #f28e1e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#71bd39">Pengumuman 10 Besar</h4>
        <p style="color:#4f4f4f" id="hilang"><i>3 September 2018</i></p>
        <span class="cd-date" style="opacity: 1">3 September 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-picture" style="background-color: #f28e1e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#71bd39">Presentasi dan Babak Final Karya Tulis</h4>
        <p style="color:#4f4f4f" id="hilang"><i>16 September 2018</i></p>
        <span class="cd-date" style="opacity: 1">16 September 2018</span>
      </div>
    </div>
  </section>
      </div>
    </section>

    <section class="portfolio" id="prize">
        <div class="container">
          <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#4f4f4f!important">Hadiah</h2>
          <hr class="star-dark mb-5" style="border-color: #4f4f4f">
          <div class="row">
            <div class="col-md-6 col-lg-4" style="margin-top: 10px">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/juara1.png" style="" data-aos="fade-up"></center>
              </a>
              <p style="text-align: center;">Uang senilai Rp. <strong>1.750.000</strong>,<br>
            Trophy, dan Sertifikat
          </p>
            </div>
            <div class="col-md-6 col-lg-4" style="margin-top: 10px">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/juara2.png" style="width: 90%;padding-top: 10%" data-aos="fade-up"></center>
              </a>
              <p style="text-align: center;">Uang senilai Rp. <strong>1.500.000</strong>,<br>
            Trophy, dan Sertifikat
          </p>
            </div>
            <div class="col-md-6 col-lg-4" style="margin-top: 10px">
               <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/juara3.png" style="width: 80%;padding-top: 20%" data-aos="fade-up"></center>
              </a>
              <p style="text-align: center;">Uang senilai Rp. <strong>1.250.000</strong>,<br>
            Trophy, dan Sertifikat
          </p>
            </div>
          </div>
        </div>
      </section>

    <section class="bg-primary text-white mb-0" style="background-color: #f28e1e!important;">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#ffffff!important">Cara Pendaftaran</h2>
        <hr class="star-light3 mb-5" style="border-color:#ffffff">   <br>
        <h3 class="text-center text-uppercase text-secondary mb-0" style="color:#71bd39!important;padding-bottom: 10px;text-shadow: 2px 2px #4f4f4f">Online</h3>
        <section class="cd-horizontal-timeline" style="margin-top: 0px;margin-bottom: 0px;padding-top: 0px;padding-bottom: 0px" data-aos="fade-down">
  <div class="timeline">
    <div class="events-wrapper">
      <div class="events">
        <ol>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="16/01/2014" class="selected" style="color:#4f4f4f">1st</a></li>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="28/02/2014" style="color:#4f4f4f">2nd</a></li>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="20/04/2014" style="color:#4f4f4f">3rd</a></li>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="20/05/2014" style="color:#4f4f4f">4th</a></li>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="09/07/2014" style="color:#4f4f4f">5th</a></li>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="30/08/2014" style="color:#4f4f4f">6th</a></li>
          <li style="color: #f28e1e"><a href="index.html#0" data-date="15/09/2014" style="color:#4f4f4f">7th</a></li>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="01/11/2014" style="color:#4f4f4f">8th</a></li>
          <li style="color: #f28e1e"><a href="index.html#0" data-date="02/12/2014" style="color:#4f4f4f">9th</a></li>
        </ol>

        <span class="filling-line" aria-hidden="true"></span>
      </div>
    </div>

    <ul class="cd-timeline-navigation" style="color:#f28e1e;">
      <li><a href="index.html#0" class="prev inactive">Prev</a></li>
      <li><a href="index.html#0" class="next">Next</a></li>
    </ul>
  </div>

  <div class="events-content">
    <ol style="color:#f28e1e;padding-left: 0px">
      <li class="selected" data-date="16/01/2014">
        <em style="display: none">January 16th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Membuka laman web osd-hmpipaum.com
        </p>
      </li>

      <li data-date="28/02/2014">
        <em style="display: none">February 28th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Memilih page LKTI untuk melakukan pendaftaran
        </p>
      </li>

      <li data-date="20/04/2014">
        <em style="display: none">March 20th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Mengisi data pada laman sesuai dengan petunjuk
        </p>
      </li>

      <li data-date="20/05/2014">
        <em style="display: none">May 20th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Menyimpan data yang sudah diisi agar bisa melakukan
            pembayaran
        </p>
      </li>

      <li data-date="09/07/2014">
        <em style="display: none">July 9th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Mengirim biaya pendaftaran pada nomor rekening Bank BRI
            <b>0555-01-022168-50-0</b> atas nama <b>PUTRI DWI RAHMASARI M</b>
        </p>
      </li>

      <li data-date="30/08/2014">
        <em style="display: none">August 30th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Mengirim bukti pembayaran dan bukti pengiriman berkas
            melalui web osd-hmpipaum.com dan mengkonfirmasi ke panitia
            apabila sudah membayar melalui web osd-hmpipaum.com
        </p>
      </li>

      <li data-date="15/09/2014">
        <em style="display: none">September 15th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Bukti pembayaran, naskah karya tulis (hardfile), naskah karya
            tulis (softfile) dalam CD dan foto berwarna 3 x 4 (3 lembar)
            dikirim via pos pada alamat <b>Kesekretariatan HMP IPA
            Gedung O8 Fakultas Matematika dan Ilmu Pengetahuan
            Alam, Universitas Negeri Malang, Jalan Semarang No. 5
            Malang 65145</b>
        </p>
      </li>

      <li data-date="01/11/2014">
        <em style="display: none">November 1st, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Konfirmasi ke Panitia melalui web osd-hmpipaum.com apabila
            sudah mengirim bukti pembayaran, berkas administrasi dan
            naskah karya tulis.
        </p>
      </li>

      <li data-date="02/12/2014">
        <em style="display: none">November 1st, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Peserta bisa mendownload e-sertifikat pada laman pendaftaran
            awal setelah babak final (babak final dilaksanakan pada tanggal
            16 September 2018).
        </p>
      </li>
    </ol>
  </div>
</section>
<br><br>
<h3 class="text-center text-uppercase text-secondary mb-0" style="color:#71bd39!important;padding-bottom: 10px;text-shadow: 2px 2px #4f4f4f">Offline</h3>
        <section class="cd-horizontal-timeline" style="margin-top: 0px;margin-bottom: 0px;padding-top: 0px;padding-bottom: 0px" data-aos="fade-down">
  <div class="timeline">
    <div class="events-wrapper">
      <div class="events">
        <ol>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="16/01/2014" class="selected" style="color:#4f4f4f">1st</a></li>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="17/02/2014" style="color:#4f4f4f">2nd</a></li>
          <li style="color:#f28e1e"><a href="index.html#0" data-date="18/11/2014" style="color:#4f4f4f">3rd</a></li>
        </ol>

        <span class="filling-line" aria-hidden="true"></span>
      </div>
    </div>

    <ul class="cd-timeline-navigation" style="color:#f28e1e;">
      <li><a href="index.html#0" class="prev inactive">Prev</a></li>
      <li><a href="index.html#0" class="next">Next</a></li>
    </ul>
  </div>

  <div class="events-content">
    <ol style="color:#f28e1e;padding-left: 0px">
      <li class="selected" data-date="16/01/2014">
        <em style="display: none">January 16th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Mengisi formulir pendaftaran dan membayar biaya pendaftaran
        </p>
      </li>

      <li data-date="17/02/2014">
        <em style="display: none">February 28th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Melengkapi persyaratan administrasi
        </p>
      </li>

      <li data-date="18/11/2014">
        <em style="display: none">March 20th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
            Menyerahkan naskah karya tulis dan berkas administrasi di
            Kesekretariatan HMP IPA Gedung O8 Lantai 1 Fakultas
            Matematika dan Ilmu Pengetahuan Alam, Universitas Negeri
            Malang. Pada Hari Senin – Jum’at, pukul 08.00 – 16.00 WIB
        </p>
      </li>
    </ol>
  </div>
</section>
      </div>
    </section>

    <section class="portfolio" id="gallery">
        <div class="container">
          <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#4f4f4f!important">Galeri</h2>
          <hr class="star-dark mb-5" style="border-color: #4f4f4f">
           <div class="row">
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-1" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #ce432c">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g7.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-2" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #ce432c">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g8.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
               <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-3" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #ce432c">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g9.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-4" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #ce432c">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g10.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-5" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #ce432c">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g11.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-6" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #ce432c">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g12.jpg">
              </a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-lg-2">
            </div>
      <div class="col-md-6 col-lg-2">
            </div>
          </div>
        </div>
      </section>
  


    <section class="portfolio" id="sponsor">
        <div class="container">
          <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#4f4f4f!important">Didukung Oleh</h2>
          <hr class="star-dark mb-5" style="border-color: #4f4f4f">
          <div class="row">
            <div class="col-md-12 col-lg-12" style="margin-top: 10px">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/gabungan-logo_1.png" style="max-width: 100%" data-aos="zoom-out"></center>
              </a>
            </div>
            <div class="col-md-6 col-lg-1" style="margin-top: 10px"></div>
          </div>
        </div>
      </section>

      <section class="portfolio" style="padding-top:0px">
          <div class="container">
              <div id="disqus_thread"></div>
              <script>
              
              /**
              *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
              *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
              /*
              var disqus_config = function () {
              this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
              this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
              };
              */
              (function() { // DON'T EDIT BELOW THIS LINE
              var d = document, s = d.createElement('script');
              s.src = 'https://lkti-um.disqus.com/embed.js';
              s.setAttribute('data-timestamp', +new Date());
              (d.head || d.body).appendChild(s);
              })();
              </script>
              <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                          
        </div>                         
      </section>
      

    <footer class="footer text-center" style="background-color: #f28e1e">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Kontak</h4>
            <p class="lead mb-0">085735927816 (Mita)
              <br>085785765573 (Putri)
              <br>085706460174 (Amri)</p>
          </div>
          <div class="col-md-6 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Sosial Media</h4>
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" style="border-width:0.15rem" href="https://www.instagram.com/hmpipa_um/">
                  <i class="fa fa-fw fa-instagram"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" style="border-width:0.15rem" href="https://www.instagram.com/osd_hmpipaum2018/">
                  <i class="fa fa-fw fa-instagram"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <div class="copyright py-4 text-center text-white" style="background-color: #ce432c">
      <div class="container">
        <small>Made with <b style="color:red">&hearts;</b> by <a href="http://olimpiade.id" style="color:black"><b>Olimpiade.id</b></a></small>
      </div>
    </div>

    <div class="scroll-to-top position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <!-- Modal 1 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-1">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#71bd39"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g7.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #71bd39;border-color: #71bd39">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 2 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-2">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#71bd39"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g8.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #71bd39;border-color: #71bd39">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 3 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-3">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#71bd39"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g9.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #71bd39;border-color: #71bd39">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 4 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-4">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#71bd39"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g10.jpg" alt="">
              <p class="mb-5"></p>
             <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #71bd39;border-color: #71bd39">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 5 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-5">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#71bd39"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g11.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #71bd39;border-color: #71bd39">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 6 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-6">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#71bd39"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g12.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #71bd39;border-color: #71bd39">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      function getTimeRemaining(endtime) {
      var t = Date.parse(endtime) - Date.parse(new Date());
      var seconds = Math.floor((t / 1000) % 60);
      var minutes = Math.floor((t / 1000 / 60) % 60);
      var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
      var days = Math.floor(t / (1000 * 60 * 60 * 24));
      return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
      };
    }

    function initializeClock(id, endtime) {
      var clock = document.getElementById(id);
      var daysSpan = clock.querySelector('.days');
      var hoursSpan = clock.querySelector('.hours');
      var minutesSpan = clock.querySelector('.minutes');
      var secondsSpan = clock.querySelector('.seconds');

      function updateClock() {
        var t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
          clearInterval(timeinterval);
        }
      }

      updateClock();
      var timeinterval = setInterval(updateClock, 1000);
    }

    var deadline = new Date(Date.parse(new Date("Aug 26, 2018 23:59:00")));
    initializeClock('clockdiv', deadline);
    </script>

    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <script src="assets/js/jqBootstrapValidation.js"></script>
    <script src="assets/js/contact_me.js"></script>
    <script src="assets/js/aos.js"></script>

    <script src="assets/js/freelancer.min.js"></script>
    <script src="assets/js/modernizr.js"></script>
    <script src="assets/js/main.js"></script>

    <script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
    <script type="text/javascript">
   jQuery(document).ready(function($){
  var timelines = $('.cd-horizontal-timeline'),
    eventsMinDistance = 60;

  (timelines.length > 0) && initTimeline(timelines);

  function initTimeline(timelines) {
    timelines.each(function(){
      var timeline = $(this),
        timelineComponents = {};

      timelineComponents['timelineWrapper'] = timeline.find('.events-wrapper');
      timelineComponents['eventsWrapper'] = timelineComponents['timelineWrapper'].children('.events');
      timelineComponents['fillingLine'] = timelineComponents['eventsWrapper'].children('.filling-line');
      timelineComponents['timelineEvents'] = timelineComponents['eventsWrapper'].find('a');
      timelineComponents['timelineDates'] = parseDate(timelineComponents['timelineEvents']);
      timelineComponents['eventsMinLapse'] = minLapse(timelineComponents['timelineDates']);
      timelineComponents['timelineNavigation'] = timeline.find('.cd-timeline-navigation');
      timelineComponents['eventsContent'] = timeline.children('.events-content');

      setDatePosition(timelineComponents, eventsMinDistance);

      var timelineTotWidth = setTimelineWidth(timelineComponents, eventsMinDistance);

      timeline.addClass('loaded');

      timelineComponents['timelineNavigation'].on('click', '.next', function(event){
        event.preventDefault();
        updateSlide(timelineComponents, timelineTotWidth, 'next');
      });

      timelineComponents['timelineNavigation'].on('click', '.prev', function(event){
        event.preventDefault();
        updateSlide(timelineComponents, timelineTotWidth, 'prev');
      });

      timelineComponents['eventsWrapper'].on('click', 'a', function(event){
        event.preventDefault();
        timelineComponents['timelineEvents'].removeClass('selected');
        $(this).addClass('selected');
        updateOlderEvents($(this));
        updateFilling($(this), timelineComponents['fillingLine'], timelineTotWidth);
        updateVisibleContent($(this), timelineComponents['eventsContent']);
      });

      timelineComponents['eventsContent'].on('swipeleft', function(){
        var mq = checkMQ();
        ( mq == 'mobile' ) && showNewContent(timelineComponents, timelineTotWidth, 'next');
      });
      timelineComponents['eventsContent'].on('swiperight', function(){
        var mq = checkMQ();
        ( mq == 'mobile' ) && showNewContent(timelineComponents, timelineTotWidth, 'prev');
      });

      $(document).keyup(function(event){
        if(event.which=='37' && elementInViewport(timeline.get(0)) ) {
          showNewContent(timelineComponents, timelineTotWidth, 'prev');
        } else if( event.which=='39' && elementInViewport(timeline.get(0))) {
          showNewContent(timelineComponents, timelineTotWidth, 'next');
        }
      });
    });
  }

  function updateSlide(timelineComponents, timelineTotWidth, string) {

    var translateValue = getTranslateValue(timelineComponents['eventsWrapper']),
      wrapperWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', ''));

    (string == 'next')
      ? translateTimeline(timelineComponents, translateValue - wrapperWidth + eventsMinDistance, wrapperWidth - timelineTotWidth)
      : translateTimeline(timelineComponents, translateValue + wrapperWidth - eventsMinDistance);
  }

  function showNewContent(timelineComponents, timelineTotWidth, string) {

    var visibleContent =  timelineComponents['eventsContent'].find('.selected'),
      newContent = ( string == 'next' ) ? visibleContent.next() : visibleContent.prev();

    if ( newContent.length > 0 ) {
      var selectedDate = timelineComponents['eventsWrapper'].find('.selected'),
        newEvent = ( string == 'next' ) ? selectedDate.parent('li').next('li').children('a') : selectedDate.parent('li').prev('li').children('a');

      updateFilling(newEvent, timelineComponents['fillingLine'], timelineTotWidth);
      updateVisibleContent(newEvent, timelineComponents['eventsContent']);
      newEvent.addClass('selected');
      selectedDate.removeClass('selected');
      updateOlderEvents(newEvent);
      updateTimelinePosition(string, newEvent, timelineComponents, timelineTotWidth);
    }
  }

  function updateTimelinePosition(string, event, timelineComponents, timelineTotWidth) {

    var eventStyle = window.getComputedStyle(event.get(0), null),
      eventLeft = Number(eventStyle.getPropertyValue("left").replace('px', '')),
      timelineWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', '')),
      timelineTotWidth = Number(timelineComponents['eventsWrapper'].css('width').replace('px', ''));
    var timelineTranslate = getTranslateValue(timelineComponents['eventsWrapper']);

        if( (string == 'next' && eventLeft > timelineWidth - timelineTranslate) || (string == 'prev' && eventLeft < - timelineTranslate) ) {
          translateTimeline(timelineComponents, - eventLeft + timelineWidth/2, timelineWidth - timelineTotWidth);
        }
  }

  function translateTimeline(timelineComponents, value, totWidth) {
    var eventsWrapper = timelineComponents['eventsWrapper'].get(0);
    value = (value > 0) ? 0 : value;
    value = ( !(typeof totWidth === 'undefined') &&  value < totWidth ) ? totWidth : value;
    setTransformValue(eventsWrapper, 'translateX', value+'px');

    (value == 0 ) ? timelineComponents['timelineNavigation'].find('.prev').addClass('inactive') : timelineComponents['timelineNavigation'].find('.prev').removeClass('inactive');
    (value == totWidth ) ? timelineComponents['timelineNavigation'].find('.next').addClass('inactive') : timelineComponents['timelineNavigation'].find('.next').removeClass('inactive');
  }

  function updateFilling(selectedEvent, filling, totWidth) {

    var eventStyle = window.getComputedStyle(selectedEvent.get(0), null),
      eventLeft = eventStyle.getPropertyValue("left"),
      eventWidth = eventStyle.getPropertyValue("width");
    eventLeft = Number(eventLeft.replace('px', '')) + Number(eventWidth.replace('px', ''))/2;
    var scaleValue = eventLeft/totWidth;
    setTransformValue(filling.get(0), 'scaleX', scaleValue);
  }

  function setDatePosition(timelineComponents, min) {
    for (i = 0; i < timelineComponents['timelineDates'].length; i++) {
        var distance = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][i]),
          distanceNorm = Math.round(distance/timelineComponents['eventsMinLapse']) + 2;
        timelineComponents['timelineEvents'].eq(i).css('left', distanceNorm*min+'px');
    }
  }

  function setTimelineWidth(timelineComponents, width) {
    var timeSpan = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][timelineComponents['timelineDates'].length-1]),
      timeSpanNorm = timeSpan/timelineComponents['eventsMinLapse'],
      timeSpanNorm = Math.round(timeSpanNorm) + 4,
      totalWidth = timeSpanNorm*width;
    timelineComponents['eventsWrapper'].css('width', totalWidth+'px');
    updateFilling(timelineComponents['timelineEvents'].eq(0), timelineComponents['fillingLine'], totalWidth);

    return totalWidth;
  }

  function updateVisibleContent(event, eventsContent) {
    var eventDate = event.data('date'),
      visibleContent = eventsContent.find('.selected'),
      selectedContent = eventsContent.find('[data-date="'+ eventDate +'"]'),
      selectedContentHeight = selectedContent.height();

    if (selectedContent.index() > visibleContent.index()) {
      var classEnetering = 'selected enter-right',
        classLeaving = 'leave-left';
    } else {
      var classEnetering = 'selected enter-left',
        classLeaving = 'leave-right';
    }

    selectedContent.attr('class', classEnetering);
    visibleContent.attr('class', classLeaving).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
      visibleContent.removeClass('leave-right leave-left');
      selectedContent.removeClass('enter-left enter-right');
    });
    eventsContent.css('height', selectedContentHeight+'px');
  }

  function updateOlderEvents(event) {
    event.parent('li').prevAll('li').children('a').addClass('older-event').end().end().nextAll('li').children('a').removeClass('older-event');
  }

  function getTranslateValue(timeline) {
    var timelineStyle = window.getComputedStyle(timeline.get(0), null),
      timelineTranslate = timelineStyle.getPropertyValue("-webkit-transform") ||
            timelineStyle.getPropertyValue("-moz-transform") ||
            timelineStyle.getPropertyValue("-ms-transform") ||
            timelineStyle.getPropertyValue("-o-transform") ||
            timelineStyle.getPropertyValue("transform");

        if( timelineTranslate.indexOf('(') >=0 ) {
          var timelineTranslate = timelineTranslate.split('(')[1];
        timelineTranslate = timelineTranslate.split(')')[0];
        timelineTranslate = timelineTranslate.split(',');
        var translateValue = timelineTranslate[4];
        } else {
          var translateValue = 0;
        }

        return Number(translateValue);
  }

  function setTransformValue(element, property, value) {
    element.style["-webkit-transform"] = property+"("+value+")";
    element.style["-moz-transform"] = property+"("+value+")";
    element.style["-ms-transform"] = property+"("+value+")";
    element.style["-o-transform"] = property+"("+value+")";
    element.style["transform"] = property+"("+value+")";
  }

  function parseDate(events) {
    var dateArrays = [];
    events.each(function(){
      var dateComp = $(this).data('date').split('/'),
        newDate = new Date(dateComp[2], dateComp[1]-1, dateComp[0]);
      dateArrays.push(newDate);
    });
      return dateArrays;
  }

  function parseDate2(events) {
    var dateArrays = [];
    events.each(function(){
      var singleDate = $(this),
        dateComp = singleDate.data('date').split('T');
      if( dateComp.length > 1 ) {
        var dayComp = dateComp[0].split('/'),
          timeComp = dateComp[1].split(':');
      } else if( dateComp[0].indexOf(':') >=0 ) {
        var dayComp = ["2000", "0", "0"],
          timeComp = dateComp[0].split(':');
      } else {
        var dayComp = dateComp[0].split('/'),
          timeComp = ["0", "0"];
      }
      var newDate = new Date(dayComp[2], dayComp[1]-1, dayComp[0], timeComp[0], timeComp[1]);
      dateArrays.push(newDate);
    });
      return dateArrays;
  }

  function daydiff(first, second) {
      return Math.round((second-first));
  }

  function minLapse(dates) {
    var dateDistances = [];
    for (i = 1; i < dates.length; i++) {
        var distance = daydiff(dates[i-1], dates[i]);
        dateDistances.push(distance);
    }
    return Math.min.apply(null, dateDistances);
  }

  function elementInViewport(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while(el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
        top < (window.pageYOffset + window.innerHeight) &&
        left < (window.pageXOffset + window.innerWidth) &&
        (top + height) > window.pageYOffset &&
        (left + width) > window.pageXOffset
    );
  }

  function checkMQ() {
    return window.getComputedStyle(document.querySelector('.cd-horizontal-timeline'), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
  }
});
    </script>

  </body>

</html>
